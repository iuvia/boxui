import Vue from 'vue'
import Router from 'vue-router'
import views from './views'
import guards from './guards'
import settings from './settings'

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: views.BoxHome,
      meta: { title: settings.PROJECT_TITLE },
      children: [
        {
          path: '',
          name: 'dashboard',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxDashboard,
          meta: { breadcrumbParent: null },
        },
        {
          path: '/launchers',
          name: 'launchers',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxLaunchers,
          meta: { breadcrumbParent: 'dashboard' },
        },
        {
          path: '/marketplace',
          name: 'marketplace',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxMarketplace,
          meta: { breadcrumbParent: 'dashboard' },
        },
        {
          path: '/settings',
          name: 'settings',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxSettings,
          meta: { breadcrumbParent: 'dashboard' },
        },
        {
          path: '/settings/sessions',
          name: 'session-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxSessionManager,
          meta: { breadcrumbParent: 'settings' },
        },
        {
          path: '/settings/device',
          name: 'device-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxDeviceManager,
          meta: { breadcrumbParent: 'settings' },
        },
        {
          path: '/settings/subscription',
          name: 'subscription-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxSubscriptionManager,
          meta: { breadcrumbParent: 'settings' },
        },
        {
          path: '/settings/apps',
          name: 'app-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxAppManager,
          meta: { breadcrumbParent: 'settings' },
        },
        {
          path: '/settings/apps/:appid',
          name: 'appinfo',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxAppManagerDetail,
          meta: { breadcrumbParent: 'app-manager' },
        },
        {
          path: '/settings/users',
          name: 'user-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxUserManager,
          meta: { breadcrumbParent: 'settings' },
        },
        {
          path: '/settings/users/profiles',
          name: 'profile-manager',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxUserProfileManager,
          meta: { breadcrumbParent: 'user-manager' },
        },
        {
          path: '/settings/users/profiles/new',
          name: 'profile-new',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxUserProfileNew,
          meta: { breadcrumbParent: 'profile-manager' },
        },
        {
          path: '/settings/users/profiles/edit/:username',
          name: 'profile-edit',
          beforeEnter: guards.isAuthenticated,
          component: views.BoxUserProfileNew,
          meta: { breadcrumbParent: 'profile-manager' },
        },
      ],
    },
    {
      path: '/login',
      name: 'login',
      // beforeEnter: guards.redirectHome,
      component: views.BoxLogin,
    },
  ],
})

router.beforeEach(guards.setTitleAndMeta)
router.beforeEach(guards.clearErrors)

export default router
