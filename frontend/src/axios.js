import axios from 'axios'
import settings from './settings'

const http = axios.create({
  baseURL: settings.URLS.API.BASE,
  timeout: settings.TIMEOUT,
  headers: {
    'Content-Type': 'application/json',
  },
})

export default http
