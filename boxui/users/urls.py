from django.urls import path
from rest_framework.routers import DefaultRouter
from knox import views as knox_views

from boxui.users import viewsets

app_name = "users"

urlpatterns = [
    path('login/', viewsets.LoginView.as_view(), name='knox_login'),
    path('logout/all/', knox_views.LogoutAllView.as_view(), name='knox_logoutall'),
    path('logout/', knox_views.LogoutView.as_view(), name='knox_logout'),
    # path("login/", view=viewsets.UserLogin.as_view(), name="login"),
    # path("logout/", view=viewsets.UserLogout.as_view(), name="logout"),
    # path("list/", view=viewsets.UserList.as_view(), name="userlist"),
    # path("redirect/", view=user_redirect_view, name="redirect"),
    # path("update/", view=user_update_view, name="update"),
    # path("~<str:username>/", view=user_detail_view, name="detail"),
]

router = DefaultRouter()
router.register(r'sessions', viewsets.SessionsView, basename='sessions')
router.register(r'', viewsets.UserList, basename='user')
urlpatterns += router.urls
