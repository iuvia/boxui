from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import get_language
from django_auth_ldap.backend import LDAPBackend, _LDAPUser
from ldap import NO_SUCH_OBJECT, SCOPE_ONELEVEL

from .models import Filter, LDAPUser, LDAPUserExists


class LDAPCreatingBackend(LDAPBackend):
    def authenticate_ldap_user(self, ldap_user, password):
        """Authenticate LDAP user but if the authentication failed, test
        whether we need to create the basic LDAP structures.

        FIXME: When the onboarding app is completed, this setup should
        be handled by the onboarding app itself.
        """
        try:
            ldap_user._authenticate_user_dn(password)
        except _LDAPUser.AuthenticationFailed:
            username = ldap_user._username or ldap_user._user.username
            self.initialize_ldap_if_required(username, password)
        return super().authenticate_ldap_user(ldap_user, password)

    def initialize_ldap_first_time(self, initial_user_name):
        """Creates the LDAP entities that need to exist prior to creating
        users. This means the associated organizationalUnit for
        holding users and groups, plus the required groups for our app
        (active/staff/superuser).

        FIXME: Use proper logging.
        """
        def create_ldap_entities(entities, kind='organizationalUnit', **kwargs):
            for entity in entities:
                try:
                    LDAPUser.objects.create(entity, objectClass=[kind.encode('utf-8')], **kwargs)
                except LDAPUserExists as e:
                    print("LDAP ALREADY_EXISTS:", entity, e)
        LDAPUser.objects.bind()
        _APP = settings.AUTH_LDAP_APP_NAME
        # Step 1: create ous
        ous = [
            settings.AUTH_LDAP_CREATEUSER_SUBTREE,
            "ou=groups,{}".format(settings.AUTH_LDAP_ROOT_DN),
            "ou={},ou=groups,{}".format(_APP, settings.AUTH_LDAP_ROOT_DN),
        ]
        # Step 2: create groups with initial user
        groups = [
            "cn=active,ou={},ou=groups,{}".format(_APP, settings.AUTH_LDAP_ROOT_DN),
            "cn=staff,ou={},ou=groups,{}".format(_APP, settings.AUTH_LDAP_ROOT_DN),
            "cn=superuser,ou={},ou=groups,{}".format(_APP, settings.AUTH_LDAP_ROOT_DN),
        ]
        create_ldap_entities(ous)
        create_ldap_entities(groups, kind='groupOfNames', member=[
            '{rdnt}={rdn},{st}'.format(
                rdnt='uid',  # TODO: Generalize
                rdn=initial_user_name,
                st=settings.AUTH_LDAP_CREATEUSER_SUBTREE,
            ).encode('utf-8')
        ])

    def initialize_ldap_if_required(self, initial_user_name, password):
        # Check for group existence
        try:
            searchdn = "ou={},ou=groups,{}".format(settings.AUTH_LDAP_APP_NAME, settings.AUTH_LDAP_ROOT_DN)
            LDAPUser.objects.bind()
            res = LDAPUser.objects.ldap.search_st(
                searchdn,
                SCOPE_ONELEVEL,
                filterstr=Filter.And(objectClass='groupOfNames', cn='superuser')
            )
            if len(res) == 1:
                return None
        except NO_SUCH_OBJECT:
            pass

        db_user = get_user_model().objects.get(username=initial_user_name, is_superuser=True)
        if db_user and db_user.check_password(password):
            self.initialize_ldap_first_time(initial_user_name)
            # Maybe user password has been updated through LDAP
            self.create_ldap_user(db_user, password)
        else:
            get_user_model().set_password(password)  # Avoid Django #20760 in this case
            raise Exception('Please ask the device owner to log in first')

    def create_ldap_user(self, django_user, user_password, **kwargs):
        LDAPUser.objects.create(
            '{rdn}={username},{trail}'.format(
                rdn='uid',
                username=django_user.username,
                trail=settings.AUTH_LDAP_CREATEUSER_SUBTREE,
            ),
            objectClass=[b'account', b'extensibleObject'],
            userPassword=user_password,
            sn=(django_user.last_name or " "),
            givenName=(django_user.first_name or " "),
            cn="{} {}".format(django_user.first_name, django_user.last_name),
            mail=django_user.email,
            preferredLanguage=get_language(),
        )
