from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.signals import user_logged_in
from django.utils import timezone
from django.utils.translation import get_language

from rest_framework import permissions, status
from rest_framework.decorators import action
from rest_framework.mixins import DestroyModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ViewSet
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken

from boxui.appimages.renderers import ImageRenderer
from config.swagger import swagger
from .models import LDAPUser, LDAPUserExists
from .serializers import (
    KnoxSessionSerializer,
    LanguageSerializer,
    LDAPUserSerializer,
    LoginSerializer,
    UserActivationSerializer
)


class UserList(ViewSet):
    """
    Lists all users, or create a new user
    """
    serializer_class = LDAPUserSerializer

    def list(self, request, format=None):
        users = LDAPUser.objects.all()
        serializer = LDAPUserSerializer(users, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        user = LDAPUser.objects.find(pk=pk)
        if user:
            serializer = LDAPUserSerializer(user, context={'request': request})
            return Response(serializer.data)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, pk=None):
        user = LDAPUser.objects.find(pk=pk)
        if user:
            LDAPUser.objects.delete(pk=pk)
            return Response({}, status=status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def partial_update(self, request, pk=None):
        user = LDAPUser.objects.find(pk=pk)
        if user:
            serializer = LDAPUserSerializer(user, data=request.data, partial=True, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                return Response({}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': 'Username was not found'}, status=status.HTTP_404_NOT_FOUND)

    def create(self, request, format=None):
        serializer = LDAPUserSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            try:
                user = serializer.save()
                LDAPUser.objects.activate(user.dn)
                return Response({}, status=status.HTTP_201_CREATED)
            except LDAPUserExists:
                return Response({'error': 'Username already exists'}, status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger(
        responses={
            200: "Return data about the authenticated user",
            500: "Internal Error probably related to the answers from the LDAP server. Contact support.",
        }
    )
    @action(detail=False, methods=('GET',))
    def me(self, request, format=None):
        if request.user.is_authenticated:
            user = LDAPUser.objects.find(pk=request.user.username)
            if not user:
                return Response(
                    {'error': 'ldap_error.auth_but_notfound'},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
            serializer = LDAPUserSerializer(user, context={'request': request})
            return Response(serializer.data)

        return Response({
            'type': str(type(request.user)),
            'username': request.user.username,
            'is_authenticated': request.user.is_authenticated,
        })

    @swagger(
        responses={
            200: "User was logged in successfully",
            403: "User credentials were not valid",
        }
    )
    @action(
        detail=False,
        methods=('POST',),
        permission_classes=(permissions.AllowAny,),
        serializer_class=LoginSerializer,
    )
    def session_login(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        user = serializer.authenticate(request=request)
        if user is not None:
            if hasattr(user, 'ldap_user'):
                login(request, user)
            return self.me(request, format)
        return Response({'status': 'NOK'}, status=status.HTTP_403_FORBIDDEN)

    @swagger(
        responses={
            200: "User avatar rendered correctly",
            404: "User not found",
        }
    )
    @action(
        detail=True,
        methods=('GET',),
        renderer_classes=(ImageRenderer,),
        url_name='users_avatar',
    )
    def avatar(self, request, pk=None):
        user = LDAPUser.objects.find(pk=pk)
        if user:
            return Response(user.avatar(), content_type="image/png")
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    @swagger(
        method='PUT',
        request_body=LanguageSerializer,
        responses={
            200: "User preferred language changed successfully",
            404: "User not found",
            400: "Validation error. Missing lang attribute in the request body"
        }
    )
    @action(
        detail=True,
        methods=('GET', 'PUT',),
    )
    def lang(self, request, pk=None):
        """
        Get or set user's preferred language in LDAP
        """
        user = LDAPUser.objects.find(pk=pk)
        if user:
            if request.method == 'GET':
                lang = user.preferredLanguage if 'preferredLanguage' in user.attributes else get_language()
                return Response({'lang': lang}, status.HTTP_200_OK)
            else:
                serialized = LanguageSerializer(data=request.data)
                if not serialized.is_valid():
                    return Response(
                        {"error": "Wrong or missing lang attribute in the request."},
                        status=status.HTTP_400_BAD_REQUEST
                    )

                lang = serialized.validated_data['lang']
                LDAPUser.objects.update(user, preferredLanguage=lang)
                return Response({'lang': lang}, status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    @swagger(
        responses={
            200: "User activated/deactivated correctly",
            404: "User not found",
            400: "Wrong format of request data",
            409: "User already active when requesting an activation, or viceversa"
        }
    )
    @action(
        detail=True,
        methods=('PUT',),
        serializer_class=UserActivationSerializer,
    )
    def activation(self, request, pk=None):
        """
        Set user activation on/off, add or remove membership from active LDAP group
        """
        user = LDAPUser.objects.find(pk=pk)
        if not user:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        serialized = UserActivationSerializer(data=request.data)
        if not serialized.is_valid():
            return Response(
                {"error": "Wrong or missing is_active attribute in the request."},
                status=status.HTTP_400_BAD_REQUEST
            )

        if serialized.validated_data['is_active']:
            try:
                LDAPUser.objects.activate(user.dn)
            except Exception:
                return Response(
                    {"error": "The user is already active"},
                    status=status.HTTP_409_CONFLICT
                )
        else:
            try:
                LDAPUser.objects.deactivate(user.dn)
            except Exception:
                return Response(
                    {"error": "The user is not active"},
                    status=status.HTTP_409_CONFLICT
                )

        return Response({}, status=status.HTTP_200_OK)

    @session_login.mapping.get
    def session_login_get(self, request, format=None):
        return Response({'hello': 'world'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @swagger(
        responses={
            200: "Boxui available languages",
        }
    )
    @action(
        detail=False,
        methods=('GET',)
    )
    def i18n(self, request, format=None):
        return Response({'langs': dict(settings.LANGUAGES)}, status=status.HTTP_200_OK)


class LoginView(KnoxLoginView):
    def post(self, request, format=None):
        token_limit_per_user = self.get_token_limit_per_user()
        if token_limit_per_user is not None:
            now = timezone.now()
            token = request.user.auth_token_set.filter(expiry__gt=now)
            if token.count() >= token_limit_per_user:
                return Response(
                    {"error": "Maximum amount of tokens allowed per user exceeded."},
                    status=status.HTTP_403_FORBIDDEN
                )
        token_ttl = self.get_token_ttl()
        instance, token = AuthToken.objects.create(request.user, token_ttl)
        user_logged_in.send(sender=request.user.__class__,
                            request=request, user=request.user, token=instance)
        data = self.get_post_response_data(request, token, instance)
        return Response(data)


class SessionsView(GenericViewSet, ListModelMixin, UpdateModelMixin, DestroyModelMixin):
    serializer_class = KnoxSessionSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'token_key'

    def get_queryset(self):
        return self.request.user.auth_token_set.all()
