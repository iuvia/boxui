import json
import pytest
from django.conf import settings
# from django.contrib.auth import get_user_model
from rest_framework.test import APIRequestFactory, force_authenticate

from boxui.users.viewsets import LoginView, SessionsView
from boxui.users.models import KnoxSessionOrigin

pytestmark = pytest.mark.django_db


def test_user_login_basic_get_token_get_sessions_destroy_sessions_removes_session(user: settings.AUTH_USER_MODEL):
    factory = APIRequestFactory()
    request = factory.post('/users/login/')  # Create token
    request.META['HTTP_USER_AGENT'] = 'Python/1 Testcase/1'
    force_authenticate(request, user=user)
    response = LoginView.as_view()(request)
    response.render()

    request = factory.get('/api/users/sessions/')
    force_authenticate(request, user=user)
    response = SessionsView.as_view({'get': 'list'})(request)
    response.render()

    pk = json.loads(response.content)[0]['token_key']
    count = user.auth_token_set.count()
    request = factory.delete('/api/users/sessions/{pk}'.format(pk=pk))
    force_authenticate(request, user=user)
    SessionsView.as_view({'delete': 'destroy'})(request, token_key=pk)
    assert user.auth_token_set.count() == count - 1
    assert KnoxSessionOrigin.objects.filter(token__user__pk=user.pk).count() == count - 1
