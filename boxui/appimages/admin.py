from django.contrib import admin

from boxui.appimages import models


@admin.register(models.AppDeveloper)
class AppDeveloperAdmin(admin.ModelAdmin):
    pass


@admin.register(models.IuviaPlatformApp)
class IuviaPlatformAppAdmin(admin.ModelAdmin):
    pass


@admin.register(models.AppImage)
class AppImageAdmin(admin.ModelAdmin):
    pass


@admin.register(models.AppLauncher)
class AppLauncherAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LauncherCondition)
class LauncherConditionAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LocalizedAppStrings)
class LocalizedAppStringsAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LocalizedLauncherStrings)
class LocalizedLauncherStringsAdmin(admin.ModelAdmin):
    pass


@admin.register(models.MaxPlatformResources)
class MaxPlatformResourcesAdmin(admin.ModelAdmin):
    pass


@admin.register(models.ExpectedPlatformResources)
class ExpectedPlatformResourcesAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Requirement)
class RequirementAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LauncherUse)
class LauncherUseAdmin(admin.ModelAdmin):
    pass
