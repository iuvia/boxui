import logging
from os import listdir, path

from django.conf import settings
from django.core.exceptions import ValidationError

from config import celery_app
from .models import AppImage, IuviaPlatformApp, DeveloperNotMatching
from iuvia.appimages import AppImageOps


LOGGER = logging.getLogger(__name__)


@celery_app.task()
def install_appimage(appimage: AppImage):
    """A celery task that installs the relevant AppImage and contacts
    the relevant iuvia.control endpoint for enabling and starting up.
    """
    appimage.install()


@celery_app.task()
def uninstall_appimage(appimage: AppImage):
    """A celery task that installs the relevant AppImage and contacts
    the relevant iuvia.control endpoint for enabling and starting up.
    """
    appimage.uninstall()


@celery_app.task()
def update_appimage(old_appimage: AppImage, new_appimage: AppImage):
    """A celery task that installs the relevant AppImage and contacts
    the relevant iuvia.control endpoint for enabling and starting up.
    """
    old_appimage.uninstall()
    new_appimage.install()
    # api = ICAPI()
    # old_appimage.install_state = AppImage.STATE_UPDATING
    # new_appimage.install_state = AppImage.STATE_INSTALLING
    # old_appimage.save()
    # new_appimage.save()
    # try:
    #     api.update_appimage(old_appimage.path, new_appimage.path)
    #     old_appimage.install_state = AppImage.STATE_UPDATING
    #     new_appimage.install_state = AppImage.STATE_INSTALLING
    #     old_appimage.save()
    #     new_appimage.save()
    # except RuntimeError:
    #     old_appimage.install_state = AppImage.STATE_INSTALLED
    #     new_appimage.install_state = AppImage.STATE_INSTALL_FAILED
    #     old_appimage.save()
    #     new_appimage.save()


@celery_app.task()
def refresh_installed_appimages():
    """ A celery task to inspect the appimage directory and fill up the
    database with the information that <AppImage> --metadata outputs
    """
    appimages = [f for f in listdir(settings.APPIMAGE_DIR) if f.endswith(".AppImage")]

    for app in appimages:
        runpath = path.join(settings.APPIMAGE_DIR, app)
        ops = AppImageOps(runpath)
        try:
            IuviaPlatformApp.get_or_create_from_ops(ops, save=True)
        except DeveloperNotMatching:
            # FIXME proper logging and returning of errors
            print('The AppImage developer does not match with the existing one', flush=True)
        except ValidationError:
            # FIXME proper logging and returning of errors
            print('The AppImage metadata does not have the correct format. It will be ignored.', flush=True)

    apps = IuviaPlatformApp.objects.all()
    return apps
