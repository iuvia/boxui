from django.utils.encoding import force_str
from django.utils.translation import gettext_lazy as _

from rest_framework.compat import coreapi, coreschema
from rest_framework.filters import BaseFilterBackend
from rest_framework.pagination import PageNumberPagination

from .models import LauncherUse


class LauncherUseTypeFilter(BaseFilterBackend):

    search_param = 'use_type'
    search_title = _('Use Type')
    search_description = _('The string identifier of a launcher use type.')

    def get_schema_fields(self, view):
        assert coreapi is not None, 'coreapi must be installed to use `get_schema_fields()`'
        assert coreschema is not None, 'coreschema must be installed to use `get_schema_fields()`'
        return [
            coreapi.Field(
                name=self.search_param,
                required=False,
                location='query',
                schema=coreschema.String(
                    title=force_str(self.search_title),
                    description=force_str(self.search_description)
                )
            )
        ]

    def filter_queryset(self, request, queryset, view):
        use_type = request.query_params.get(self.search_param, None)
        rev_use_types = dict([(y.lower(), x) for x, y in LauncherUse.USE_TYPES])
        if use_type in rev_use_types.keys():
            return queryset.filter(
                users_use__user=request.user,
                users_use__use_type=rev_use_types[use_type]
            )
        return queryset


class LauncherListPagination(PageNumberPagination):
    page_size = 10
