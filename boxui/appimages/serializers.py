from rest_framework.reverse import reverse
from rest_framework.serializers import (
    BooleanField,
    CharField,
    ModelSerializer,
    Serializer,
    HyperlinkedRelatedField,
    ValidationError,
    UUIDField,
    DateTimeField,
)

from boxui.appimages.models import (
    IuviaPlatformApp,
    AppDeveloper,
    AppImage,
    AppLauncher,
    LauncherCondition,
    LauncherUse,
    Requirement,
)


class AppImageHyperlinkField(HyperlinkedRelatedField):
    view_name = 'appimages:appimages_read'

    def use_pk_only_optimization(self):
        return False

    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'codename': obj.codename.codename,
            'version': obj.version
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class AppDeveloperSerializer(ModelSerializer):
    class Meta:
        model = AppDeveloper
        fields = '__all__'


class LauncherConditionSerializer(ModelSerializer):
    class Meta:
        model = LauncherCondition
        fields = ['operator']


class AppLauncherSerializer(ModelSerializer):
    appimage = AppImageHyperlinkField(read_only=True)

    action_type = CharField()
    action_params = CharField()
    conditions = LauncherConditionSerializer(many=True)

    # Translated fields
    name = CharField(source='name_l10n', read_only=True)
    description = CharField(source='description_l10n', read_only=True)

    class Meta:
        model = AppLauncher
        fields = [
            'appimage',
            'conditions',
            'platform',
            'action_type',
            'action_params',
            'name',
            'description',
            'lang',
            'icon_sizes',
            'icon_urls',
        ]


class RequirementSerializer(ModelSerializer):
    class Meta:
        model = Requirement
        fields = ['codename', 'min_version', 'max_version']


class PlatformResourcesSerializer(Serializer):
    cpu = CharField()
    ram = CharField()


class AppImageSerializer(ModelSerializer):
    launchers = AppLauncherSerializer(many=True)
    requires = RequirementSerializer(many=True)
    expected_resources = PlatformResourcesSerializer()
    max_resources = PlatformResourcesSerializer()

    # Translated fields
    name = CharField(source='name_l10n', read_only=True)
    short_description = CharField(source='short_description_l10n', read_only=True)
    long_description = CharField(source='long_description_l10n', read_only=True)

    is_installed = BooleanField()

    class Meta:
        model = AppImage
        fields = [
            'id',
            'codename',
            'metadata_version',
            'version',
            'certificate',
            'icon_sizes',
            'icon_urls',
            'l10n_default',
            'license_code',
            'license_text',
            'install_state',
            'path',
            'launchers',
            'requires',
            'expected_resources',
            'max_resources',
            'name',
            'short_description',
            'long_description',
            'is_installed',
        ]


class IuviaPlatformAppSerializer(ModelSerializer):
    appimages = AppImageSerializer(many=True)
    developer = AppDeveloperSerializer()
    latest = AppImageHyperlinkField(read_only=True)
    installed = AppImageHyperlinkField(read_only=True)

    class Meta:
        model = IuviaPlatformApp
        fields = ['appimages', 'codename', 'auto_update', 'developer', 'latest', 'installed']


class LauncherUseSerializer(Serializer):
    use_type = CharField(required=True)
    value = BooleanField(required=False, write_only=True)

    user = CharField(read_only=True)
    launcher = UUIDField(read_only=True)
    timestamp = DateTimeField(read_only=True)

    def validate_use_type(self, value):
        use_dict = {y.lower(): x for x, y in LauncherUse.USE_TYPES}
        if value.lower() not in use_dict.keys():
            raise ValidationError("use_type is not a valid Launcher use type")
        return use_dict[value.lower()]
